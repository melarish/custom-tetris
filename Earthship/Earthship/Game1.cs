using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Earthship
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Texture2D blockSprite;
        Texture2D background;
        // Rectangle for the main game screen.
        Rectangle mainFrame;
        // for drawing landed blocks
        Rectangle blockDrawRectangle;
        // current falling block
        Block block;
        List<Block> landedBlocks = new List<Block>();
        // for checking if a block has landed on top of existing ones. Defaults to the bottom of the game area
        int[] topLayer = new int[GameConstants.NUM_COLS];
        // keeps track of where blocks are
        bool[][] gameArea = new bool[GameConstants.NUM_ROWS][];
        Random r = new Random();

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            // set resolution
            graphics.PreferredBackBufferWidth = GameConstants.WINDOW_WIDTH;
            graphics.PreferredBackBufferHeight = GameConstants.WINDOW_HEIGHT;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // initialise empty game area and top layer
            for (int i = 0; i < topLayer.Length; i++) {
                topLayer[i] = GameConstants.NUM_ROWS - 1;
            }
            for (int i = 0; i < GameConstants.NUM_ROWS; i++)
            {
                gameArea[i] = new bool[GameConstants.NUM_COLS];
                for (int j = 0; j < GameConstants.NUM_COLS; j++)
                {
                    gameArea[i][j] = false;
                }
            }
            blockSprite = Content.Load<Texture2D>("block");
            background = Content.Load<Texture2D>("background");
            mainFrame = new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height);
            block = new Block(blockSprite);
            blockDrawRectangle = new Rectangle(0, 0, blockSprite.Width, blockSprite.Height);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            KeyboardState keyboard = Keyboard.GetState();

            block.Update(gameTime, keyboard, topLayer);
            if (block.Landed)
            {
                //landedBlocks.Add(block);
                topLayer[block.Column]--; // top layer has changed
                //for (int i = 0; i < gameArea.Length; i++)
                //{
                //    for (int j = 0; j < gameArea[i].Length; j++)
                //    {
                //        Console.Write(gameArea[i][j]);
                //    }
                //    Console.WriteLine("block landed");
                //}
                //Console.WriteLine(block.Row + ":" + block.Column);
                gameArea[block.Row][block.Column] = true; // remember for drawing
                //for (int i = 0; i < gameArea.Length; i++)
                //{
                //    for (int j = 0; j < gameArea[i].Length; j++)
                //    {
                //        Console.Write(gameArea[i][j]);
                //    }
                //    Console.WriteLine("block landed gamearea changed");
                //}
                block.reset();
                //Console.WriteLine("topLayer");
                for (int i = 0; i < topLayer.Length; i++)
                {                    
                    Console.Write("-"+topLayer[i]);
                }
                Console.WriteLine("");
                //block = new Block(blockSprite); // spawn new block

                // check for complete rows and delete
                List<int> rowsToDelete = new List<int>();
                for (int i = 0; i < gameArea.Length; i++)
                {
                    bool fullRow = true;
                    for (int j = 0; j < gameArea[i].Length; j++)
                    {
                        if (!gameArea[i][j])
                        {
                            fullRow = false;
                            break;
                        }
                    }
                    if (fullRow)
                    {
                        rowsToDelete.Add(i);
                    }
                }
                if (rowsToDelete.Count > 0)
                {
                    for (int i = rowsToDelete[0]; i > 0; i--) // currently one row can be completed at a time
                    {
                        gameArea[i] = gameArea[i - 1];
                        //for (int k = 0; k < gameArea.Length; k++)
                        //{
                        //    for (int j = 0; j < gameArea[k].Length; j++)
                        //    {
                        //        //Console.Write(gameArea[k][j]);
                        //    }
                        //    //Console.WriteLine();
                        //}       
                    }
                    // top layer is now lower down and topmost game area resets to empty
                    gameArea[0] = new bool[GameConstants.NUM_COLS];
                    for (int i = 0; i < topLayer.Length; i++)
                    {
                        topLayer[i]++;
                        gameArea[0][i] = false;
                    }
                }
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin();
            // Draw the background.
            spriteBatch.Draw(background, mainFrame, Color.White);
            block.Draw(spriteBatch);
            //foreach (Block b in landedBlocks)
            //{
            //    b.Draw(spriteBatch);
            //}

            // draws blocks where specified in gameArea
            for (int i = 0; i < gameArea.Length; i++)
            {
                for (int j = 0; j < gameArea[i].Length; j++)
                {
                    if (gameArea[i][j])
                    {
                        blockDrawRectangle.X = j * blockSprite.Width + GameConstants.BLOCK_DISPLAY_OFFSET;
                        blockDrawRectangle.Y = i * blockSprite.Height + GameConstants.BLOCK_DISPLAY_OFFSET;
                        spriteBatch.Draw(blockSprite, blockDrawRectangle, Color.White);
                    }
                }
            }
            spriteBatch.End();


            base.Draw(gameTime);
        }
    }
}
