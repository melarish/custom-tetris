﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Earthship
{
    /// <summary>
    /// All the constants used in the game
    /// </summary>
    public static class GameConstants
    {
        // resolution
        public const int WINDOW_WIDTH = 800;
        public const int WINDOW_HEIGHT = 500;

        // projectile characteristics
        public const float TEDDY_BEAR_PROJECTILE_SPEED = 0.3f;

        // display support
        public const int BLOCK_DISPLAY_OFFSET = 10;
        public const int NUM_COLS = 4;
        public const int NUM_ROWS = 4;
        const int DISPLAY_OFFSET = 35;
        public const string SCORE_PREFIX = "Score: ";
        public static readonly Vector2 SCORE_LOCATION =
            new Vector2(DISPLAY_OFFSET, DISPLAY_OFFSET);
        public const string HEALTH_PREFIX = "Health: ";
        public static readonly Vector2 HEALTH_LOCATION =
            new Vector2(DISPLAY_OFFSET, 2 * DISPLAY_OFFSET);

        // spawn location support
        public const int SPAWN_BORDER_SIZE = 100;
    }
}
