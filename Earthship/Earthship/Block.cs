﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Earthship
{
    /// <summary>
    /// An animated explosion object
    /// </summary>
    public class Block
    {
        #region Fields

        Rectangle drawRectangle;
        Texture2D image;
        int column;
        int row;
        bool landed;
        // keeps track of when to move block downward
        int blockTimer;
        // so the block doesn't move more than once when key pressed
        bool leftKeyDown;
        bool rightKeyDown;
        // to prevent pressing Down on a newly spanwed block
        bool canUseDown;
        
        #endregion

        #region Constructors

        /// <summary>
        /// Constructs a new block object
        /// </summary>
        /// <param name="image">the sprite for the block</param>
        /// <param name="x">the x location of the center of the block</param>
        /// <param name="y">the y location of the center of the block</param>
        public Block(Texture2D image)
        {
            this.image = image;
            drawRectangle = new Rectangle(GameConstants.BLOCK_DISPLAY_OFFSET, GameConstants.BLOCK_DISPLAY_OFFSET, image.Width, image.Height);
            reset();
        }

        public void reset()
        {
            column = 0;
            row = 0;
            leftKeyDown = false;
            rightKeyDown = false;
            canUseDown = false;
            landed = false;
        }

        #endregion

        #region Properties

        public bool Landed
        {
            get { return landed; }
        }

        public int Column
        {
            get { return column; }
        }

        public int Row
        {
            get { return row; }
        }

        #endregion

        #region Private properties
        #endregion

        #region Public methods

        /// <summary>
        /// Updates the block. This only has an effect if the block is moving
        /// </summary>
        /// <param name="gameTime">the game time</param>
        public void Update(GameTime gameTime, KeyboardState keyboard, int[] topLayer)
        {                     
            if (keyboard.IsKeyDown(Keys.Left) && !leftKeyDown && column > 0)
            {
                column--;
                leftKeyDown = true;
            }
            if (keyboard.IsKeyDown(Keys.Right) && !rightKeyDown && column < GameConstants.NUM_COLS - 1)
            {
                column++;
                rightKeyDown = true;
            }
            if (keyboard.IsKeyUp(Keys.Left))
            {
                leftKeyDown = false;
            }
            if (keyboard.IsKeyUp(Keys.Right))
            {
                rightKeyDown = false;
            }
            // bring block down instantly
            if (keyboard.IsKeyDown(Keys.Down) && canUseDown) // so you don't accidentally bring down 2 blocks at once
            {
                row = topLayer[column];
                Console.WriteLine("row:" + row);
                blockTimer = 1000;
            }
            blockTimer += gameTime.ElapsedGameTime.Milliseconds;
            if (blockTimer >= 500)
            {
                canUseDown = true;
                blockTimer = 0;
                // check if landed on an existing block, or the bottom
                if (row < topLayer[column])
                {
                    row++;
                }
                else
                {
                    landed = true;
                }
            }
        }

        /// <summary>
        /// Draws the explosion. This only has an effect if the explosion animation is playing
        /// </summary>
        /// <param name="spriteBatch">the spritebatch</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            drawRectangle.X = GameConstants.BLOCK_DISPLAY_OFFSET + column * image.Width;
            drawRectangle.Y = GameConstants.BLOCK_DISPLAY_OFFSET + row * image.Height;
            spriteBatch.Draw(image, drawRectangle, Color.White);
        }

        #endregion

        #region Private methods

        
        #endregion

    }
}
